const express = require('express');
const sizeOf = require('image-size');
const multer = require('multer');
const fileExtension = require('file-extension');
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, './uploads');
  },
  filename: (req, file, callback) => {
    const id = Date.now();
    const ext = fileExtension(file.originalname);
    callback(null, `${file.fieldname}-${id}.${ext}`);
  }
});

const upload = multer({ storage }).single('img');

router.post('/images', (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.end('Error uploading file.');
    }
    const dimensions = sizeOf(req.file.path);
    res.send({
      res: 'Image uploaded!',
      mime: req.file.mimetype,
      path: req.file.path,
      width: dimensions.width,
      height: dimensions.height,
      size: req.file.size
    });
    res.end();
  });
});

module.exports = router;

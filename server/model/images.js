var mongoose = require('mongoose');

var imgSchema = new mongoose.Schema({
  path: String,
  width: Number,
  height: Number,
  ext: String
});

mongoose.model('Image', imgSchema);
